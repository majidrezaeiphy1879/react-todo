import "./App.css";
import { useState } from "react";
import notodo from "./images/undraw_to_do_re_jaef.svg";
import Body from "./components/Body";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import { TodosContext } from "./context/TodosContext";
import TodoList from "./components/TodoList";

import Figure from 'react-bootstrap/Figure';
import CheckAll from "./components/CheckAll";
import TodoDetail from "./components/TodoDetail";
function App() {
  const [todos, setTodos] = useState([
    { id: 1, title: "Task one", isComplete: false, editing: false },
    { id: 2, title: "Task two", isComplete: false, editing: false },
  ]);
  const [todoId, setTodoId] = useState(3);
  const [todoInput, setTodoInput] = useState("");

  const deleteTodo = (id) => {
    setTodos([...todos].filter((todo) => todo.id !== id))
  }
  const completeTodo = (id) => {
    const updatedTodos = todos.map((todo) => {
      if (todo.id === id) {
        todo.isComplete = !todo.isComplete;
      }
      return todo;
    })
    setTodos(updatedTodos);
  }


  const [filter, setFilter] = useState('all');
  const todosFiltered = () => {
    if (filter === 'all') { return todos }
    else if (filter === 'active') { return todos.filter(todo => !todo.isComplete) }
    else if (filter === 'completed') { return todos.filter(todo => todo.isComplete) }
  }
  return (
    <TodosContext.Provider value={{
      todos, setTodos, todoId, setTodoId,
      deleteTodo, completeTodo,
      todoInput, setTodoInput, filter, setFilter, todosFiltered
    }}>
      <Container className="main-container">
        <Row>
          <Col>
            <Body />
            {todos.length === 0 ? (<div style={{ display: 'flex', justifyContent: 'center' }}><Figure.Image src={notodo} className="notodo-image" alt='no todos' /></div>)
              : (<><TodoList />
                <CheckAll /><TodoDetail /></>)}
          </Col>
        </Row>
      </Container>
    </TodosContext.Provider>
  );
}

export default App;
