import React from "react";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Button from "react-bootstrap/Button";
import { TodosContext } from "../context/TodosContext";
import { useContext } from "react";

function CheckAll() {
  const { todos, setTodos } = useContext(TodosContext);
  const completeAll = () => {
    const updatedTodos = todos.map((todo) => {
      todo.isComplete = true;

      return todo;
    });
    setTodos(updatedTodos);
  };
  return (
    <Container div className="check-all-container">
      <Row>
        <Col style={{display:'flex', flexDirection:'row', justifyContent:'space-between', alignItems:'center'}}>
          <Button onClick={completeAll}>Check-All</Button>
         <span className="todoparagraph">{todos.filter(todo=>!todo.isComplete).length>1 ? `${todos.filter(todo=>!todo.isComplete).length} Tasks Remaining`:
         `${todos.filter(todo=>!todo.isComplete).length} Task Remaining`}</span> 
        </Col>
      </Row>
    </Container>
  );
}

export default CheckAll;
