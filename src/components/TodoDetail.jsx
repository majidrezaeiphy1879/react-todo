import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Button from "react-bootstrap/Button";
import { useContext } from "react";
import { TodosContext } from "../context/TodosContext";
function TodoDetail() {
  const { todos, setTodos,todosFiltered, setFilter} = useContext(TodosContext);
  const clearItems = () => {
    setTodos([...todos].filter((todo) => !todo.isComplete));
  };

  return (
    <Container className="todo-detail">
      <Row>
        <Col>
          <Button style={{ margin: "5px" }} filter={'all'} onClick={()=>{setFilter('all'); todosFiltered()}}>All</Button>
          <Button style={{ margin: "5px" }} filter={'active'} onClick={()=>{setFilter('active'); todosFiltered()}}>Active</Button>
          <Button style={{ margin: "5px" }} filter={'completed'} onClick={()=>{setFilter('completed'); todosFiltered()}}>Completed</Button>
        </Col>
        <Col style={{ display: "flex", justifyContent: "end" }}>
          <Button style={{ margin: "5px" }} onClick={clearItems}>
            Clear
          </Button>
        </Col>
      </Row>
    </Container>
  );
}

export default TodoDetail;
