import React, { useContext, useEffect, useRef } from "react";
import { TodosContext } from "../context/TodosContext";
import Form from "react-bootstrap/Form";

function Body() {
  
  const { todos, setTodos, todoId, setTodoId,setTodoInput,todoInput } = useContext(TodosContext);
  const handleInput = (event) => {
    setTodoInput(event.target.value);
  };
  const addTodo = (event) => {
    event.preventDefault();
    if (todoInput.trim().length === 0) {
      return;
    }
    setTodos((todos) => [
      ...todos,
      { id: todoId, title: todoInput, isComplete: false },
    ]);
    setTodoId((prevId) => prevId + 1);
    setTodoInput("");
  };
  const inputF = useRef(null)
  useEffect(()=>{
    inputF.current.focus();
  },[])
  return (
    <div>
      <Form onSubmit={addTodo}>
        <Form.Group className="mb-3" >
          <Form.Label className="form-label">Todos</Form.Label>
          <Form.Control
            type="text"
            placeholder="Todos"
            value={todoInput}
            onChange={handleInput}
            ref={inputF}
          />
        </Form.Group>
        {todos.length!==0 && (<Form.Text className="text-muted ps-2">
          To edit your todos double-click on them
        </Form.Text>)}
      </Form>
    </div>
  );
}

export default Body;
