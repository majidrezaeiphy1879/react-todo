import React, { useContext } from "react";
import Button from "react-bootstrap/Button";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import { TodosContext } from "../context/TodosContext";
import deleteButton from "../images/delete.png";
import Form from "react-bootstrap/Form";

function TodoList() {
  const { todos, setTodos, deleteTodo, completeTodo, todosFiltered } = useContext(TodosContext);
  //editing function
  const startEditing = (id) => {
    const updatedTodos = todos.map((todo) => {
      if (todo.id === id) {
        todo.editing = true;
      }
      return todo;
    });
    setTodos(updatedTodos);
  };
  const updateTodos = (e, id) => {
    const updatedTodos = todos.map((todo) => {
      if (todo.id === id) {
        if (e.target.value.trim().length === 0) {
          todo.editing = false;
          return todo;
        }
        todo.title = e.target.value;
        todo.editing = false;
      }
      return todo;
    });
    setTodos(updatedTodos);
  };
  
  return (
    <div style={{ marginTop: "10px" }}>
      {todosFiltered().map((todo, index) => {
        return (
          <Container key={todo.id}>
            <Row>
              <Col className="todos-col-style">
                <input
                  type="checkbox"
                  onChange={() => completeTodo(todo.id)}
                  checked={todo.isComplete ? true : false}
                />
                {!todo.editing ? (
                  <span
                    onDoubleClick={() => startEditing(todo.id)}
                    className={`todoparagraph ${
                      todo.isComplete ? "todo-done-style " : ""
                    }`}
                  >
                    {todo.title}
                  </span>
                ) : (
                  <Form.Control
                    type="text"
                    defaultValue={todo.title}
                    autoFocus
                    className="editing-input"
                    onBlur={(e) => updateTodos(e, todo.id)}
                    onKeyDown={(e) => {
                      if (e.key === "Enter") {
                        updateTodos(e, todo.id);
                      }
                    }}
                  />
                )}
                <Button variant="light" onClick={() => deleteTodo(todo.id)}>
                  <img
                    src={deleteButton}
                    alt="Delete"
                    style={{ height: "18px", width: "18px" }}
                  />
                </Button>
              </Col>
            </Row>
          </Container>
        );
      })}
    </div>
  );
}

export default TodoList;
